import maximagev2 as creator
from duckduckgo_search import ddg_images
import glob, os
from bottle import route, run, post, request

@route("/")
def index():
    return "<h3>Ask an image to Maximage</h3><form action='/' method='POST'><input type='text' placeholder='Imagine...' name='query'><br><input value='1' type='number' name='stepx'><label>Step x</label><br><input value='1' type='number' name='stepy'><label>Step y</label><br><input type='number' value='15' name='precision'><label>Precision (Warning : too much precision can make an ugly image)</label><br><input type='number' value='500' name='height'><label>Height of the image</label><br><input type='number' value='500' name='width'><label>Width of the image</label><p>If you don't know what is Step X and StepY, please don't touch them</p><input type='submit' value='Submit to maximage'></form>"

@post("/")
def create():
    keywords = request.forms.get("query")
    print(keywords)
    r = ddg_images('"'+keywords+'"', max_results=int(request.forms.get("precision")), download=True, size="Large", license_image="Public")
    path=glob.glob("ddg_images_'"+keywords+"'*")
    file=creator.generate((path[0]+"/"+file for file in os.listdir(path[0])), stepx=int(request.forms.get("stepx")), stepy=int(request.forms.get("stepy")), name=keywords, height=int(request.forms.get("height")), width=int(request.forms.get("width")))
    for rmfile in os.listdir(path[0]):
        os.remove(path[0]+"/"+rmfile)
    os.rmdir(path[0])
    return "<h1>"+keywords+"</h1><br><img src='/img/"+file.replace("/", '-')+"'><br>"+index()

    
@route("/img/<img>")
def img(img):
    return open(img.replace('-', "/"), "rb").read()


run(host="maximage", port=8080)

