import random
from PIL import Image
        
def generate(imagesFiles, stepx=1, stepy=1, name="", height=500, width=500):
    print("Processing request")
    images=[]
    for image in imagesFiles:
        try:
            img=Image.open(image)
        except:
            print("Can't recognize image.")
        
    for img in images:
        pass

    img=Image.new("RGB", (height, width))
    pos=random.randint(0, 100)
    step=random.randint(1, 10)
    lastpixel=(0, 0, 0)
    pickup=getPickup(images, 0, 0, step, lastpixel)
    imagesNum=len(images)
    for x in range(width):
        if x%stepx==0:
            
            pickup=random.choice(images)
        for y in range(height):
            if y%stepy==0:
                pickup=random.choice(images)
            try:
                lastpixel=pickup.getpixel((x, y))
                img.putpixel((x, y), lastpixel)
            except:
                pass
            #put(x, y, img, images)
            pos+=step
    file="img/maximage_"+name.replace(" ", "_")+"_"+str(random.randint(0, 1000000000000000))+".jpg"
    img.save(file)
    return file
